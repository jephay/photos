/**
 * @author Jeffrey Yang, Yuxiang Wang
 */

package photos.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import photos.app.Album;
import photos.app.Photo;
import photos.app.Photo.Tag;
import photos.app.Photos;
import photos.app.User;
import photos.app.UserManager;

/**
 * Controller to manage the search photo scene
 */
public class SearchController {
	/**
	 * <code>User</code> instance that owns the photos being searched
	 */
	User user;

	@FXML
	Button backButton;
	@FXML
	Button logoutButton;
	@FXML
	Button searchButton;
	@FXML
	ListView<Photo> listView;
	@FXML
	TextField startDateInput;
	@FXML
	TextField endDateInput;
	@FXML
	TextField tagInput;
	@FXML
	Button createAlbumButton;

	private ObservableList<Photo> obsList;

	public void start(Stage mainStage, User user, UserManager userManager) throws Exception {
		this.user = user;

		obsList = FXCollections.observableArrayList(new ArrayList<Photo>());
		listView.setItems(obsList);
		
		createAlbumButton.setDisable(true);

		listView.setCellFactory(param -> new ListCell<Photo>() {
			{
				setStyle("-fx-padding: 10 0 10 0; -fx-margin: 0");
			}

			@Override
			protected void updateItem(Photo item, boolean empty) {
				super.updateItem(item, empty);

				if (empty || item == null) {
					setGraphic(null);
				} else {
					HBox listItem = new HBox();
					listItem.setStyle("-fx-padding: 0 0 0 10");
					listItem.setPrefWidth(0.9 * listView.getWidth());

					ImageView thumbnail = new ImageView();
					thumbnail.setFitHeight(listItem.getPrefWidth() * 0.15);
					thumbnail.setFitWidth(listItem.getPrefWidth() * 0.15);
					thumbnail.setPreserveRatio(true);
					thumbnail.setImage(item.getImage());

					Label fileNameLabel = new Label();
					fileNameLabel.setText(item.getFilename());
					fileNameLabel.setStyle("-fx-padding: 0 0 0 10");
					fileNameLabel.setPrefWidth(listItem.getPrefWidth() * 0.20);
					fileNameLabel.setTextAlignment(TextAlignment.LEFT);

					Label dateLabel = new Label();
					dateLabel.setText(item.getTimestamp(false));
					dateLabel.setStyle("-fx-padding: 0 0 0 10");
					dateLabel.setPrefWidth(listItem.getPrefWidth() * 0.15);
					dateLabel.setTextAlignment(TextAlignment.LEFT);

					Label tagsLabel = new Label();
					tagsLabel.setWrapText(true);
					tagsLabel.setStyle("-fx-padding: 0 0 0 10");
					tagsLabel.setPrefWidth(listItem.getPrefWidth() * 0.5);
					tagsLabel.setTextAlignment(TextAlignment.LEFT);

					ArrayList<Tag> tags = item.getTags();
					String tagsString = "";
					for (Tag tag : tags) {
						tagsString += tag.getName() + ": ";
						for (String value : tag.getValues()) {
							tagsString += value + ", ";
						}
						tagsString = tagsString.substring(0, tagsString.length() - 2) + "\n";
					}
					tagsLabel.setText(tagsString);

					listItem.getChildren().addAll(thumbnail, fileNameLabel, dateLabel, tagsLabel);

					setGraphic(listItem);
				}
			}
		});

		backButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				Photos.switchToAlbumSelect(mainStage, user, userManager);
			}
		});

		logoutButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				Photos.switchToLogin(mainStage, userManager);
			}
		});

		searchButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				handleSearch(mainStage);
			}
		});

		createAlbumButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				handleCreateAlbum(mainStage, userManager);
			}
		});
	}

	/**
	 * EventHandler function to handle searching the user's list of photos
	 * 
	 * @param stage <code>Stage</code> instance the current scene is running on
	 */
	public void handleSearch(Stage stage) {
		// get search queries
		SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");
		String startDateString = startDateInput.getText();
		String endDateString = endDateInput.getText();
		Date startDate = null;
		Date endDate = null;
		if (startDateString.length() > 0 || endDateString.length() > 0) {
			try {
				startDate = format1.parse(startDateString);
				endDate = format1.parse(endDateString);
			} catch (Exception e) {
				e.printStackTrace();
				Photos.showError(stage, "Invalid dates", "Please follow date format");
				return;
			}
		}

		String tagQueryString = tagInput.getText().toLowerCase().trim();
		String keyword = "";
		String[] tag1 = null;
		String[] tag2 = null;
		if (tagQueryString.length() > 0) {
			String[] keywords = { "or", "and" };
			String query1 = "";
			String query2 = "";
			for (String k : keywords) {
				int keywordIndex = tagQueryString.indexOf(" " + k + " ");
				if (keywordIndex > -1) {
					query1 = tagQueryString.substring(0, keywordIndex);
					query2 = tagQueryString.substring(keywordIndex + k.length() + 1);
					keyword = k;
					break;
				}
			}
			if (query2.length() == 0)
				query1 = tagQueryString;

			tag1 = query1.split("=");
			if (query2.length() > 0)
				tag2 = query2.split("=");
		}

		if (startDate == null && tag1 == null) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.initOwner(stage);
			alert.setTitle("No input");
			alert.setHeaderText("Please enter a date range or tag to search");

			alert.showAndWait();
			return;
		}

		// search user's photos
		listView.getItems().clear();
		ArrayList<Photo> photos = user.getPhotos();
		for (Photo photo : photos) {
			if (startDate != null && !(photo.getDate().after(startDate) && photo.getDate().before(endDate))) {
				continue;
			}

			if (keyword.equals("or") && !(photo.containsTag(tag1[0], tag1[1]) || photo.containsTag(tag2[0], tag2[1]))) {
				continue;
			} else if (keyword.equals("and")
					&& !(photo.containsTag(tag1[0], tag1[1]) && photo.containsTag(tag2[0], tag2[1]))) {
				continue;
			} else if (keyword.equals("") && tag1 != null && !photo.containsTag(tag1[0], tag1[1])) {
				continue;
			}
			obsList.add(photo);
		}
		
		if(obsList.size() == 0) {
			createAlbumButton.setDisable(true);
		}
		else
			createAlbumButton.setDisable(false);
	}

	/**
	 * EventHandler function to handle when an album is created from the search
	 * results
	 * 
	 * @param stage       <code>Stage</code> instance the current scene is running
	 *                    on
	 * @param userManager global <code>UserManager</code> instance
	 */
	public void handleCreateAlbum(Stage stage, UserManager userManager) {
		// create a popup dialog
		Dialog<ButtonType> createAlbumDialog = new Dialog<ButtonType>();
		createAlbumDialog.setTitle("Create album");
		ButtonType submit = new ButtonType("Ok", ButtonData.OK_DONE);
		ButtonType cancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		createAlbumDialog.getDialogPane().getButtonTypes().add(cancel);
		createAlbumDialog.getDialogPane().getButtonTypes().add(submit);

		DialogPane dialogPane = createAlbumDialog.getDialogPane();
		dialogPane.setPrefWidth(300);
		dialogPane.setPrefHeight(100);

		HBox dialogContainer = new HBox();
		dialogContainer.setPadding(new Insets(10));
		dialogContainer.setPrefWidth(dialogPane.getPrefWidth());
		dialogContainer.setPrefHeight(dialogPane.getPrefHeight());
		dialogContainer.setAlignment(Pos.CENTER);

		Label albumNameLabel = new Label("Album name:");
		TextField albumNameInput = new TextField();
		albumNameInput.setPadding(new Insets(0, 0, 0, 5));
		HBox.setHgrow(albumNameInput, Priority.ALWAYS);
		dialogContainer.getChildren().add(albumNameLabel);
		dialogContainer.getChildren().add(albumNameInput);

		dialogPane.setContent(dialogContainer);
		Optional<ButtonType> result = createAlbumDialog.showAndWait();
		if (result.isPresent() && result.get() == submit) {
			// valid input
			String name = albumNameInput.getText();
			if (name.length() == 0) {
				Photos.showError(stage, "Invalid album name", "Please enter an album name.");
				return;
			}

			Album newAlbum = new Album(name);
			for (Photo photo : obsList)
				newAlbum.addPhoto(photo);
			user.addAlbum(newAlbum);
			Photos.switchToAlbumSelect(stage, user, userManager);
		}
	}
}
