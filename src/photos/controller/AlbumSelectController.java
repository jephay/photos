/**
 * @author Jeffrey Yang, Yuxiang Wang
 */

package photos.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import photos.app.Album;
import photos.app.Photos;
import photos.app.User;
import photos.app.UserManager;

/**
 * Controller to manage the album select scene
 */
public class AlbumSelectController {
	/**
	 * <code>User</code> instance that owns the albums displayed
	 */
	User user;

	@FXML
	Button logoutButton;
	@FXML
	Button createAlbumButton;
	@FXML
	Button searchButton;
	@FXML
	VBox rightCol;
	@FXML
	VBox albumDisplay;
	@FXML
	ListView<Album> listView;
	@FXML
	Label albumLabel;
	@FXML
	Label countLabel;
	@FXML
	Label firstDateLabel;
	@FXML
	Label lastDateLabel;
	@FXML
	Button openButton;
	@FXML
	Button editButton;
	@FXML
	Button deleteButton;

	Label emptyListLabel;
	private ObservableList<Album> obsList;

	public void start(Stage mainStage, User user, UserManager userManager) throws Exception {
		this.user = user;

		emptyListLabel = new Label("No albums yet");
		emptyListLabel.setMinHeight(albumDisplay.getPrefHeight());
		obsList = FXCollections.observableList(user.getAlbums());
		listView.setItems(obsList);

		// set listener for items
		listView.getSelectionModel().selectedIndexProperty().addListener((obs, oldVal, newVal) -> showAlbum());

		// select first item
		listView.getSelectionModel().select(0);

		// check if list is empty
		if (obsList.size() == 0) {
			openButton.setDisable(true);
			editButton.setDisable(true);
			deleteButton.setDisable(true);

			rightCol.getChildren().remove(0);
			rightCol.getChildren().add(0, emptyListLabel);
		}

		logoutButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				Photos.switchToLogin(mainStage, userManager);
			}
		});

		createAlbumButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				handleCreateAlbum(mainStage);
			}
		});

		searchButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				Photos.switchToSearch(mainStage, user, userManager);
			}
		});

		openButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				Photos.switchToManageAlbum(mainStage, user, listView.getSelectionModel().getSelectedItem(),
						userManager);
			}
		});

		editButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				handleEditAlbum(mainStage);
			}
		});

		deleteButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				handleDeleteAlbum(mainStage);
			}
		});
	}

	/**
	 * EventHandler function to handle when an album is selected
	 * 
	 */
	public void showAlbum() {
		if(obsList.size() == 0)
			return;
		Album album = listView.getSelectionModel().getSelectedItem();
		int photoCount = album.getPhotos().size();

		albumLabel.setText(album.toString());
		countLabel.setText(Integer.toString(photoCount));
		if (photoCount > 0) {
			Date[] dateRange = album.getDateRange();
			SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");
			firstDateLabel.setText(format1.format(dateRange[0]));
			lastDateLabel.setText(format1.format(dateRange[1]));
		} else {
			firstDateLabel.setText("N/A");
			lastDateLabel.setText("N/A");
		}
	}

	/**
	 * EventHandler function to handle when an album is created
	 * 
	 * @param stage <code>Stage</code> instance the current scene is running on
	 * 
	 */
	public void handleCreateAlbum(Stage stage) {
		// create a popup dialog
		Dialog<ButtonType> createAlbumDialog = new Dialog<ButtonType>();
		createAlbumDialog.setTitle("Create album");
		ButtonType submit = new ButtonType("Ok", ButtonData.OK_DONE);
		ButtonType cancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		createAlbumDialog.getDialogPane().getButtonTypes().add(cancel);
		createAlbumDialog.getDialogPane().getButtonTypes().add(submit);

		DialogPane dialogPane = createAlbumDialog.getDialogPane();
		dialogPane.setPrefWidth(300);
		dialogPane.setPrefHeight(100);

		HBox dialogContainer = new HBox();
		dialogContainer.setPadding(new Insets(10));
		dialogContainer.setPrefWidth(dialogPane.getPrefWidth());
		dialogContainer.setPrefHeight(dialogPane.getPrefHeight());
		dialogContainer.setAlignment(Pos.CENTER);

		Label albumNameLabel = new Label("Album name:");
		TextField albumNameInput = new TextField();
		albumNameInput.setPadding(new Insets(0, 0, 0, 5));
		HBox.setHgrow(albumNameInput, Priority.ALWAYS);
		dialogContainer.getChildren().add(albumNameLabel);
		dialogContainer.getChildren().add(albumNameInput);

		dialogPane.setContent(dialogContainer);
		Optional<ButtonType> result = createAlbumDialog.showAndWait();
		if (result.isPresent() && result.get() == submit) {
			// valid input
			String name = albumNameInput.getText();
			if (name.length() == 0) {
				Photos.showError(stage, "Invalid album name", "Please enter an album name.");
				return;
			}

			Album newAlbum = new Album(name);
			if(rightCol.getChildren().get(0) == emptyListLabel) {
				rightCol.getChildren().remove(0);
				rightCol.getChildren().add(0, albumDisplay);
			}
			obsList.add(newAlbum);
			listView.getSelectionModel().select(obsList.indexOf(newAlbum));

			openButton.setDisable(false);
			editButton.setDisable(false);
			deleteButton.setDisable(false);
		}
	}

	/**
	 * EventHandler function to handle when an album is edited
	 * 
	 * @param stage <code>Stage</code> instance the current scene is running on
	 */
	public void handleEditAlbum(Stage stage) {
		Album album = listView.getSelectionModel().getSelectedItem();

		// create a popup dialog
		Dialog<ButtonType> editAlbumDialog = new Dialog<ButtonType>();
		editAlbumDialog.setTitle("Edit album");
		ButtonType submit = new ButtonType("Ok", ButtonData.OK_DONE);
		ButtonType cancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		editAlbumDialog.getDialogPane().getButtonTypes().add(cancel);
		editAlbumDialog.getDialogPane().getButtonTypes().add(submit);

		DialogPane dialogPane = editAlbumDialog.getDialogPane();
		dialogPane.setPrefWidth(300);
		dialogPane.setPrefHeight(100);

		HBox dialogContainer = new HBox();
		dialogContainer.setPadding(new Insets(10));
		dialogContainer.setPrefWidth(dialogPane.getPrefWidth());
		dialogContainer.setPrefHeight(dialogPane.getPrefHeight());
		dialogContainer.setAlignment(Pos.CENTER);

		Label albumNameLabel = new Label("Album name:");
		TextField albumNameInput = new TextField();
		albumNameInput.setText(album.toString());
		HBox.setHgrow(albumNameInput, Priority.ALWAYS);
		dialogContainer.getChildren().add(albumNameLabel);
		dialogContainer.getChildren().add(albumNameInput);

		dialogPane.setContent(dialogContainer);
		Optional<ButtonType> result = editAlbumDialog.showAndWait();
		if (result.isPresent() && result.get() == submit) {
			String name = albumNameInput.getText().trim();
			album.setName(name);
			listView.refresh();
			albumLabel.setText(name);
		}
	}

	/**
	 * EventHandler function to handle when an album is deleted
	 * 
	 * @param stage <code>Stage</code> instance the current scene is running on
	 * 
	 */
	public void handleDeleteAlbum(Stage stage) {
		Album album = listView.getSelectionModel().getSelectedItem();

		// confirm remove with alert
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.initOwner(stage);
		alert.setTitle("Delete album");
		alert.setHeaderText("Are you sure you want to delete " + album + "?");

		Optional<ButtonType> result = alert.showAndWait();

		// get alert result
		if (result.get() == ButtonType.OK) {
			obsList.remove(album);
		}
		
		// check if list is empty
		if (obsList.size() == 0) {
			openButton.setDisable(true);
			editButton.setDisable(true);
			deleteButton.setDisable(true);

			rightCol.getChildren().remove(0);
			rightCol.getChildren().add(0, emptyListLabel);
		}
	}
}
