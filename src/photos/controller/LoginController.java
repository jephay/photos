/**
 * @author Jeffrey Yang, Yuxiang Wang
 */

package photos.controller;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import photos.app.Photos;
import photos.app.User;
import photos.app.UserManager;

/**
 * Controller to manage the login scene
 */
public class LoginController {
	@FXML
	TextField usernameInput;
	@FXML
	TextField passwordInput;
	@FXML
	Button submitButton;

	public void start(Stage mainStage, UserManager userManager) throws Exception {
		submitButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				handleLogin(mainStage, userManager);
			}
		});
	}

	/**
	 * EventListener function to handle login
	 * 
	 * @param stage       <code>Stage</code> instance the current scene is running
	 *                    on
	 * @param userManager global <code>UserManager</code> instance
	 */
	public void handleLogin(Stage stage, UserManager userManager) {

		// get input for username and password
		String username = usernameInput.getText();
		String password = passwordInput.getText();

		// check if admin
		if (username.equals("admin")) {
			if (password.equals("admin")) {
				Photos.switchToManageUsers(stage, userManager);
				return;
			}
		} else {
			for (User user : userManager.getUsers()) {
				if (user.toString().equals(username) && user.getPassword().equals(password)) {
					Photos.switchToAlbumSelect(stage, user, userManager);
					return;
				}
			}
		}
		Alert alert = new Alert(AlertType.WARNING);
		alert.initOwner(stage);
		alert.setTitle("Failed login");
		alert.setHeaderText("Username or password is invalid");
		alert.showAndWait();
		return;
	}

}
