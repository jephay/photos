/**
 * @author Jeffrey Yang, Yuxiang Wang
 */

package photos.controller;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import photos.app.Album;
import photos.app.Photo;
import photos.app.Photo.Tag;
import photos.app.Photos;
import photos.app.User;
import photos.app.UserManager;

/**
 * Controller to manage the manage album scene
 */
public class ManageAlbumController {
	/**
	 * <code>User</code> instance that owns the album being displayed
	 */
	User user;

	/**
	 * <code>Album</code> instance that is being displayed
	 */
	Album album;

	/**
	 * variable to track index of selected photo
	 */
	int selectedPhoto = 0;

	@FXML
	Button backButton;
	@FXML
	Button logoutButton;
	@FXML
	ScrollPane scrollPane;
	@FXML
	Label albumLabel;
	@FXML
	TilePane tilePane;
	@FXML
	Button prevButton;
	@FXML
	Button nextButton;
	@FXML
	Button addPhotoButton;
	@FXML
	ImageView imageView;
	@FXML 
	VBox photoDisplay;
	@FXML
	ScrollPane photoScrollPane;
	@FXML
	Label filenameLabel;
	@FXML
	Label dateLabel;
	@FXML
	Label captionLabel;
	@FXML
	ListView<Tag> tagList;
	@FXML
	Button copyButton;
	@FXML
	Button moveButton;
	@FXML
	Button editButton;
	@FXML
	Button removeButton;

	Label emptyListLabel;
	private ObservableList<Tag> obsList;

	public void start(Stage mainStage, User user, Album album, UserManager userManager) throws Exception {
		this.user = user;
		this.album = album;

		emptyListLabel = new Label("No photos yet");
		
		albumLabel.setText(album.toString());

		tilePane.setPadding(new Insets(5));
		tilePane.prefWidthProperty().bind(scrollPane.prefWidthProperty());
		tilePane.setVgap(10);
		tilePane.setHgap(10);
		tilePane.setPrefColumns(4);

		tagList.setCellFactory(param -> new ListCell<Tag>() {
			{
				setStyle("-fx-padding: 10 0 10 0; -fx-margin: 0");
			}

			@Override
			protected void updateItem(Tag item, boolean empty) {
				super.updateItem(item, empty);

				if (empty || item == null) {
					setGraphic(null);
				} else {
					HBox listItem = new HBox();
					listItem.setPrefWidth(0.9 * tagList.getWidth());

					Label tagName = new Label();
					tagName.setText(item.getName());
					tagName.setStyle("-fx-padding: 0 0 0 10");
					tagName.setPrefWidth(listItem.getPrefWidth() * 0.4);
					tagName.setTextAlignment(TextAlignment.LEFT);

					Label tagValues = new Label();
					ArrayList<String> values = item.getValues();
					String valuesString = "";
					for (String value : values) {
						valuesString += value + "\n";
					}
					valuesString = valuesString.substring(0, valuesString.length() - 1);

					tagValues.setText(valuesString);
					tagValues.setStyle("-fx-padding: 0 0 0 10");
					tagValues.setPrefWidth(listItem.getPrefWidth() * 0.6);
					tagValues.setTextAlignment(TextAlignment.LEFT);

					listItem.getChildren().addAll(tagName, tagValues);

					setGraphic(listItem);
				}
			}
		});

		loadPhotos();

		backButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				Photos.switchToAlbumSelect(mainStage, user, userManager);
			}
		});

		logoutButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				Photos.switchToLogin(mainStage, userManager);
			}
		});

		prevButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				handleSelectPhoto(selectedPhoto - 1);
			}
		});

		nextButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				handleSelectPhoto(selectedPhoto + 1);
			}
		});

		addPhotoButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				handleAddPhoto(mainStage);
			}
		});

		copyButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				handleCopyPhoto(mainStage);
			}
		});

		moveButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				handleMovePhoto(mainStage);
			}
		});

		editButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				handleEditPhoto(mainStage);
			}
		});

		removeButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				handleRemovePhoto(mainStage);
			}
		});
	}

	/**
	 * Function to load all the album's photos into display
	 */
	public void loadPhotos() {
		tilePane.getChildren().clear();

		for (Photo photo : album.getPhotos()) {
			BorderPane photoContainer = new BorderPane();
			photoContainer.setPrefWidth(90);
			photoContainer.setPrefHeight(120);

			ImageView thumbnail = new ImageView();
			thumbnail.setFitHeight(photoContainer.getPrefWidth());
			thumbnail.setFitWidth(photoContainer.getPrefWidth());
			thumbnail.setPreserveRatio(true);
			thumbnail.setImage(photo.getImage());

			Label captionLabel = new Label();
			captionLabel.setText(photo.getCaption());

			photoContainer.getChildren().add(thumbnail);
			photoContainer.setBottom(captionLabel);
			BorderPane.setAlignment(captionLabel, Pos.CENTER);

			photoContainer.setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					int id = tilePane.getChildren().indexOf(photoContainer);
					handleSelectPhoto(id);
				}
			});

			tilePane.getChildren().add(photoContainer);
		}
		if (album.getPhotos().size() == 0) {
			prevButton.setDisable(true);
			nextButton.setDisable(true);
			copyButton.setDisable(true);
			moveButton.setDisable(true);
			editButton.setDisable(true);
			removeButton.setDisable(true);
			
			imageView.setImage(null);
			photoDisplay.getChildren().remove(1);
			photoDisplay.getChildren().add(1, emptyListLabel);
		} else {
			handleSelectPhoto(selectedPhoto);
		}
	}

	/**
	 * EventHandler function to handle when a photo is selected
	 * 
	 * @param id index of selected photo
	 */
	public void handleSelectPhoto(int id) {
		if (id >= album.getPhotos().size())
			id = 0;
		if (id < 0)
			id = album.getPhotos().size() - 1;

		if (selectedPhoto > -1 && selectedPhoto < album.getPhotos().size())
			tilePane.getChildren().get(selectedPhoto).setStyle("-fx-border-color: transparent; -fx-border-width: 0;");

		selectedPhoto = id;
		BorderPane photoContainer = (BorderPane) tilePane.getChildren().get(id);
		photoContainer.setStyle(
				"-fx-border-color: red; -fx-border-style: solid; -fx-border-width: 3; -fx-border-insets: -3;");

		Photo photo = album.getPhotos().get(id);
		Image img = photo.getImage();
		imageView.setImage(img);
		filenameLabel.setText(photo.getFilename());
		dateLabel.setText(photo.getTimestamp(true).toString());
		captionLabel.setText(photo.getCaption());
		obsList = FXCollections.observableList(photo.getTags());
		tagList.setItems(obsList);
	}

	/**
	 * EventHandler function to handle when a photo is added to the album
	 * 
	 * @param stage <code>Stage</code> instance the current scene is running on
	 */
	public void handleAddPhoto(Stage stage) {
		// get photo from file selector
		FileDialog fileDialog = new FileDialog((Frame) null, "Select File to Open");
		fileDialog.setFile("*.jpg;*.jpeg;*.png;*.gif;*.bmp;");
		fileDialog.setMode(FileDialog.LOAD);
		fileDialog.setVisible(true);
		// no file selected
		if (fileDialog.getFiles().length == 0)
			return;
		File file = fileDialog.getFiles()[0];

		// check if selected file was image
		String fileName = file.getPath();
		String extension = "";
		int i = fileName.lastIndexOf('.');
		int p = Math.max(fileName.lastIndexOf('/'), fileName.lastIndexOf('\\'));
		if (i > p) {
			extension = fileName.substring(i + 1).toLowerCase();
		}
		if (!(extension.equals("jpg") || extension.equals("jpeg") || extension.equals("png") || extension.equals("gif") || extension.equals("bmp"))) {
			Photos.showError(stage, "Invalid file", "Please choose an image file");
			return;
		}
		Image image = new Image(file.toURI().toString(), 100, 100, false, false);

		// create a popup dialog
		Dialog<ButtonType> addPhotoDialog = new Dialog<ButtonType>();
		addPhotoDialog.setTitle("Add photo");
		ButtonType submit = new ButtonType("Ok", ButtonData.OK_DONE);
		ButtonType cancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		addPhotoDialog.getDialogPane().getButtonTypes().add(cancel);
		addPhotoDialog.getDialogPane().getButtonTypes().add(submit);

		DialogPane dialogPane = addPhotoDialog.getDialogPane();
		dialogPane.setPrefWidth(500);
		dialogPane.setPrefHeight(200);

		HBox dialogContainer = new HBox();
		dialogContainer.setPadding(new Insets(10));
		dialogContainer.setPrefWidth(dialogPane.getPrefWidth());
		dialogContainer.setPrefHeight(dialogPane.getPrefHeight());
		dialogContainer.setAlignment(Pos.CENTER);

		ImageView preview = new ImageView();
		preview.setFitHeight(150);
		preview.setFitWidth(150);
		preview.setPreserveRatio(true);
		preview.setImage(image);

		VBox inputContainer = new VBox();
		inputContainer.setAlignment(Pos.CENTER);
		inputContainer.setPadding(new Insets(0, 0, 0, 20));
		inputContainer.setPrefWidth(0.7 * dialogContainer.getPrefWidth());
		inputContainer.setPrefHeight(dialogContainer.getPrefHeight());

		Label filenameLabel = new Label("Filename: " + file.getName());
		filenameLabel.setPrefWidth(inputContainer.getPrefWidth());

		HBox captionContainer = new HBox();
		captionContainer.setPrefWidth(inputContainer.getPrefWidth());
		Label captionLabel = new Label("Caption: ");
		TextField captionInput = new TextField();
		HBox.setHgrow(captionInput, Priority.ALWAYS);
		captionContainer.getChildren().add(captionLabel);
		captionContainer.getChildren().add(captionInput);

		inputContainer.getChildren().add(filenameLabel);
		inputContainer.getChildren().add(captionContainer);
		dialogContainer.getChildren().add(preview);
		dialogContainer.getChildren().add(inputContainer);

		dialogPane.setContent(dialogContainer);
		Optional<ButtonType> result = addPhotoDialog.showAndWait();
		if (result.isPresent() && result.get() == submit) {
			String caption = captionInput.getText().trim();
			Photo photo = new Photo(file.getPath(), caption);
			// check if photo exists
			if (!user.addPhoto(photo)) {
				Photos.showError(stage, "Invalid photo", "Photo already exists");
				return;
			}
			album.addPhoto(photo);
			prevButton.setDisable(false);
			nextButton.setDisable(false);
			copyButton.setDisable(false);
			moveButton.setDisable(false);
			editButton.setDisable(false);
			removeButton.setDisable(false);
			
			photoDisplay.getChildren().remove(1);
			photoDisplay.getChildren().add(1, photoScrollPane);
			loadPhotos();
		}
	}

	/**
	 * EventHandler function to handle when a photo is copied to another album
	 * 
	 * @param stage <code>Stage</code> instance the current scene is running on
	 */
	public void handleCopyPhoto(Stage stage) {
		Photo photo = album.getPhotos().get(selectedPhoto);

		List<String> choices = new ArrayList<String>();
		for (Album a : user.getAlbums()) {
			if (a == album)
				continue;

			choices.add(a.toString());
		}

		ChoiceDialog<String> dialog = new ChoiceDialog<>("Choose album", choices);
		dialog.setTitle("Copy photo");
		dialog.setHeaderText("Copy photo to different album");
		dialog.setContentText("Choose your album:");

		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			for (Album a : user.getAlbums()) {
				if (a.toString().equals(result.get())) {
					a.addPhoto(photo);
					break;
				}
			}
		}
	}

	/**
	 * EventHandler function to handle when a photo is moved to another album
	 * 
	 * @param stage <code>Stage</code> instance the current scene is running on
	 */
	public void handleMovePhoto(Stage stage) {
		Photo photo = album.getPhotos().get(selectedPhoto);

		List<String> choices = new ArrayList<String>();
		for (Album a : user.getAlbums()) {
			if (a == album)
				continue;

			choices.add(a.toString());
		}

		ChoiceDialog<String> dialog = new ChoiceDialog<>("Choose album", choices);
		dialog.setTitle("Move photo");
		dialog.setHeaderText("Move photo to different album");
		dialog.setContentText("Choose your album:");

		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			album.removePhoto(photo);
			loadPhotos();
			for (Album a : user.getAlbums()) {
				if (a.toString().equals(result.get())) {
					a.addPhoto(photo);
					break;
				}
			}
		}
	}

	/**
	 * EventHandler function to handle when a photo is edited
	 * 
	 * @param stage <code>Stage</code> instance the current scene is running on
	 */
	public void handleEditPhoto(Stage stage) {
		Photo photo = album.getPhotos().get(selectedPhoto);

		// create a popup dialog
		Dialog<ButtonType> editPhotoDialog = new Dialog<ButtonType>();
		editPhotoDialog.setTitle("Edit photo");
		ButtonType submit = new ButtonType("Ok", ButtonData.OK_DONE);
		ButtonType cancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		editPhotoDialog.getDialogPane().getButtonTypes().add(cancel);
		editPhotoDialog.getDialogPane().getButtonTypes().add(submit);

		DialogPane dialogPane = editPhotoDialog.getDialogPane();
		dialogPane.setPrefWidth(600);
		dialogPane.setPrefHeight(350);

		VBox dialogContainer = new VBox();
		dialogContainer.setPrefWidth(dialogPane.getPrefWidth());
		dialogContainer.setPrefHeight(dialogPane.getPrefHeight());
		dialogContainer.setAlignment(Pos.CENTER);

		HBox topContainer = new HBox();
		topContainer.setPrefWidth(dialogContainer.getPrefWidth());

		HBox imageContainer = new HBox();
		imageContainer.setPadding(new Insets(0, 20, 0, 10));
		imageContainer.setAlignment(Pos.CENTER);
		ImageView preview = new ImageView();
		preview.setFitHeight(150);
		preview.setFitWidth(150);
		preview.setPreserveRatio(true);
		preview.setImage(photo.getImage());
		imageContainer.getChildren().add(preview);

		VBox inputContainer = new VBox();
		inputContainer.setAlignment(Pos.CENTER);
		inputContainer.setPrefWidth(0.7 * dialogContainer.getPrefWidth());
		inputContainer.setPrefHeight(dialogContainer.getPrefHeight());

		Label filenameLabel = new Label("Filename: " + photo.getFilename());
		filenameLabel.setPrefWidth(inputContainer.getPrefWidth());

		HBox captionContainer = new HBox();
		captionContainer.setPadding(new Insets(10, 0, 10, 0));
		captionContainer.setPrefWidth(inputContainer.getPrefWidth());
		Label captionLabel = new Label("Caption: ");
		TextField captionInput = new TextField();
		captionInput.setText(photo.getCaption());
		HBox.setHgrow(captionInput, Priority.ALWAYS);
		captionContainer.getChildren().addAll(captionLabel, captionInput);

		ListView<Tag> editTagList = new ListView<Tag>();
		editTagList.setPrefWidth(inputContainer.getPrefWidth());
		ObservableList<Tag> tempObsList = FXCollections.observableList(photo.getTags());
		editTagList.setItems(tempObsList);
		editTagList.setCellFactory(param -> new ListCell<Tag>() {
			{
				setStyle("-fx-padding: 10 0 10 0; -fx-margin: 0");
			}

			@Override
			protected void updateItem(Tag item, boolean empty) {
				super.updateItem(item, empty);

				if (empty || item == null) {
					setGraphic(null);
				} else {
					HBox listItem = new HBox();
					listItem.setPrefWidth(0.9 * editTagList.getPrefWidth());

					Label tagNameLabel = new Label();
					tagNameLabel.setText(item.getName());
					tagNameLabel.setStyle("-fx-padding: 0 0 0 10");
					tagNameLabel.setTextAlignment(TextAlignment.LEFT);
					tagNameLabel.setPrefWidth(0.4 * editTagList.getPrefWidth());

					VBox tagValues = new VBox();
					tagValues.setPrefWidth(0.6 * editTagList.getPrefWidth());
					ArrayList<String> values = item.getValues();
					for (String value : values) {
						HBox tagValueContainer = new HBox();
						tagValueContainer.setPrefWidth(tagValues.getPrefWidth());
						Label tagValueLabel = new Label(value);
						tagValueLabel.setPrefWidth(0.8 * tagValueContainer.getPrefWidth());
						Button removeTagButton = new Button("X");
						removeTagButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
							@Override
							public void handle(MouseEvent event) {
								item.removeValue(value);
								if (item.getValues().size() == 0) {
									tempObsList.remove(item);
								}
								editTagList.refresh();
							}
						});

						tagValueContainer.getChildren().addAll(tagValueLabel, removeTagButton);
						tagValues.getChildren().add(tagValueContainer);
					}
					tagValues.setStyle("-fx-padding: 0 0 0 10");

					listItem.getChildren().addAll(tagNameLabel, tagValues);

					setGraphic(listItem);
				}
			}
		});

		HBox addTagContainer = new HBox();
		addTagContainer.setPadding(new Insets(10, 0, 0, 0));
		addTagContainer.setPrefWidth(inputContainer.getPrefWidth());

		TextField tagNameInput = new TextField();
		tagNameInput.setPromptText("Tag name");
		TextField tagValueInput = new TextField();
		tagValueInput.setPromptText("Tag value");
		Button addTagButton = new Button("Add tag");
		addTagButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				String tagName = tagNameInput.getText().toLowerCase().trim();
				String tagValue = tagValueInput.getText().toLowerCase().trim();
				// validate input
				if (tagName.length() == 0 || tagValue.length() == 0) {
					Photos.showError(stage, "Invalid tag", "Tag name or value is empty.");
					return;
				}

				for (Tag tag : tempObsList) {
					if (tag.getName().equals(tagName)) {
						// tag already exists
						if (tag.getValues().contains(tagValue)) {
							Photos.showError(stage, "Invalid tag", "Tag already exists.");
							return;
						}

						tag.addValue(tagValue);
						editTagList.refresh();
						tagNameInput.setText("");
						tagValueInput.setText("");
						return;
					}
				}
				ArrayList<String> values = new ArrayList<String>();
				values.add(tagValue);
				Tag newTag = new Tag(tagName, values);
				tempObsList.add(newTag);
				tagNameInput.setText("");
				tagValueInput.setText("");
			}
		});
		MenuButton dropdown = new MenuButton("Preset tags");
		ArrayList<MenuItem> options = new ArrayList<MenuItem>();
		for (String tag : user.getTags()) {
			MenuItem item = new MenuItem(tag);
			item.setOnAction(event -> {
				tagNameInput.setText(item.getText());
			});
			options.add(item);
		}
		dropdown.getItems().clear();
		dropdown.getItems().addAll(options);
		HBox.setHgrow(tagNameInput, Priority.ALWAYS);
		HBox.setHgrow(tagValueInput, Priority.ALWAYS);
		addTagContainer.getChildren().addAll(dropdown, tagNameInput, tagValueInput, addTagButton);

		inputContainer.getChildren().addAll(filenameLabel, captionContainer, editTagList);
		topContainer.getChildren().addAll(imageContainer, inputContainer);
		dialogContainer.getChildren().addAll(topContainer, addTagContainer);

		dialogPane.setContent(dialogContainer);
		Optional<ButtonType> result = editPhotoDialog.showAndWait();
		if (result.isPresent() && result.get() == submit) {
			ArrayList<Tag> tags = new ArrayList<Tag>(tempObsList);
			photo.updateTags(tags, user);

			String caption = captionInput.getText().trim();
			photo.setCaption(caption);

			tagList.refresh();

			loadPhotos();
		}
	}

	/**
	 * EventHandler function to handle when a photo is removed from the album
	 * 
	 * @param stage <code>Stage</code> instance the current scene is running on
	 */
	public void handleRemovePhoto(Stage stage) {
		Photo photo = album.getPhotos().get(selectedPhoto);

		// confirm remove with alert
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.initOwner(stage);
		alert.setTitle("Remove song");
		alert.setHeaderText("Are you sure you want to remove " + photo.getFilename() + " from " + album + "?");

		Optional<ButtonType> result = alert.showAndWait();

		// get alert result
		if (result.get() == ButtonType.OK) {
			album.removePhoto(photo);
			if (photo.getCount() == 0) {
				user.removePhoto(photo);
			}
			if (selectedPhoto == album.getPhotos().size())
				selectedPhoto -= 1;
			loadPhotos();
		}
	}
}
