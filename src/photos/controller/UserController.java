/**
 * @author Jeffrey Yang, Yuxiang Wang
 */
package photos.controller;

import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import photos.app.Photos;
import photos.app.User;
import photos.app.UserManager;

/**
 * Controller to manage the manage user scene
 */
public class UserController {
	/**
	 * global <code>UserManager</code> instance
	 */
	UserManager userManager;

	@FXML
	Button logoutButton;
	@FXML
	ListView<User> listView;
	@FXML
	Button deleteButton;
	@FXML
	Button newUserButton;

	private ObservableList<User> obsList;

	public void start(Stage mainStage, UserManager userManager) throws Exception {
		this.userManager = userManager;
		obsList = FXCollections.observableList(userManager.getUsers());
		listView.setItems(obsList);

		// select first item
		listView.getSelectionModel().select(0);
		// list is empty
		if (listView.getSelectionModel().getSelectedItem() == null) {
			deleteButton.setDisable(true);
		}

		logoutButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				Photos.switchToLogin(mainStage, userManager);
			}
		});

		deleteButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				handleDeleteUser(mainStage);
			}
		});

		newUserButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				handleCreateUser(mainStage);
			}
		});
	}

	/**
	 * EventHandler function to handle when a user is deleted
	 * 
	 * @param stage <code>Stage</code> instance the current scene is running on
	 */
	public void handleDeleteUser(Stage stage) {
		User user = listView.getSelectionModel().getSelectedItem();

		// confirm remove with alert
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.initOwner(stage);
		alert.setTitle("Delete user");
		alert.setHeaderText("Are you sure you want to delete " + user + "?");

		Optional<ButtonType> result = alert.showAndWait();

		// get alert result
		if (result.get() == ButtonType.OK) {
			obsList.remove(user);
			// list is empty
			if (listView.getSelectionModel().getSelectedItem() == null)
				deleteButton.setDisable(true);
		}
	}

	/**
	 * EventHandler function to handle when a user is created
	 * 
	 * @param stage <code>Stage</code> instance the current scene is running on
	 */
	public void handleCreateUser(Stage stage) {
		// create a popup dialog
		Dialog<ButtonType> createUserDialog = new Dialog<ButtonType>();
		createUserDialog.setTitle("Create user");
		ButtonType submit = new ButtonType("Ok", ButtonData.OK_DONE);
		ButtonType cancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		createUserDialog.getDialogPane().getButtonTypes().add(cancel);
		createUserDialog.getDialogPane().getButtonTypes().add(submit);

		DialogPane dialogPane = createUserDialog.getDialogPane();
		dialogPane.setPrefWidth(300);
		dialogPane.setPrefHeight(100);

		VBox dialogContainer = new VBox();
		dialogContainer.setPadding(new Insets(10));
		dialogContainer.setPrefWidth(dialogPane.getPrefWidth());
		dialogContainer.setPrefHeight(dialogPane.getPrefHeight());
		dialogContainer.setAlignment(Pos.CENTER);

		HBox usernameContainer = new HBox();
		Label usernameLabel = new Label("Username:");
		usernameLabel.setPrefWidth(70);
		TextField usernameInput = new TextField();
		HBox.setHgrow(usernameInput, Priority.ALWAYS);
		usernameContainer.getChildren().add(usernameLabel);
		usernameContainer.getChildren().add(usernameInput);
		usernameContainer.setPadding(new Insets(0, 0, 10, 0));

		HBox passwordContainer = new HBox();
		Label passwordLabel = new Label("Password:");
		passwordLabel.setPrefWidth(70);
		TextField passwordInput = new TextField();
		HBox.setHgrow(passwordInput, Priority.ALWAYS);
		passwordContainer.getChildren().add(passwordLabel);
		passwordContainer.getChildren().add(passwordInput);

		dialogContainer.getChildren().add(usernameContainer);
		dialogContainer.getChildren().add(passwordContainer);

		dialogPane.setContent(dialogContainer);
		Optional<ButtonType> result = createUserDialog.showAndWait();
		if (result.isPresent() && result.get() == submit) {
			String name = usernameInput.getText().trim();
			String password = passwordInput.getText().trim();
			
			if(name.length() == 0 || password.length() == 0) {
				Photos.showError(stage, "Invalid user", "Username or password is empty");
				return;
			}
			
			User newUser = new User(name, password);
			if (!userManager.checkUsernameAvailable(newUser)) {
				Photos.showError(stage, "Invalid user", "Username already exists");
				return;
			}
			obsList.add(newUser);
			deleteButton.setDisable(false);
		}
	}
}
