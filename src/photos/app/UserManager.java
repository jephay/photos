/**
 * @author Jeffrey Yang, Yuxiang Wang
 */
package photos.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Class to hold and manage all users
 */
public class UserManager implements Serializable {
	private static final long serialVersionUID = -1993726729514889103L;

	/**
	 * List of <code>User</code> instances representing all registered users
	 */
	ArrayList<User> users;

	public static final String storeDir = "data";
	public static final String storeFile = "users.dat";

	/**
	 * Default <code>UserManager</code> constructor
	 */
	public UserManager() {
		this.users = new ArrayList<User>();

		// add default user
		User stockUser = new User("stock", "stock");
		users.add(stockUser);
		Album stockAlbum = new Album("stock");
		File stockDirectory = new File("data/stock");
		int count = 1;
		for (final File file : stockDirectory.listFiles()) {
			Photo stockPhoto = new Photo(file.getPath(), "Stock photo " + count);
			stockAlbum.addPhoto(stockPhoto);

			stockUser.addPhoto(stockPhoto);

			count += 1;
		}

		stockUser.addAlbum(stockAlbum);
	}

	/**
	 * Getter function to get a list of users
	 * 
	 * @return <code>ArrayList</code> of <code>User</code> instances representing
	 *         all registered users
	 */
	public ArrayList<User> getUsers() {
		return users;
	}

	/**
	 * Function to check if username exists in list of users
	 * 
	 * @param newUser <code>User</code> instance indicating new user to add
	 * @return boolean indicating whether the username of the new user already
	 *         exists
	 */
	public boolean checkUsernameAvailable(User newUser) {
		for (User user : users) {
			if (user.toString().equals(newUser.toString()))
				return false;
		}
		return true;
	}

	/**
	 * Function to add a new user to the list of users
	 * 
	 * @param newUser <code>User</code> instance indicating new user to add
	 * @return boolean indicating whether the user was successfully added
	 */
	public boolean addUser(User newUser) {
		if (!checkUsernameAvailable(newUser))
			return false;
		users.add(newUser);
		return true;
	}

	/**
	 * Function to remove a user from the list of users
	 * 
	 * @param removedUser <code>User</code> instance indicating the user to remove
	 */
	public void removeUser(User removedUser) {
		users.remove(removedUser);
	}

	/**
	 * Static function to serialize a <code>UserManager</code> instance
	 * 
	 * @param userManager <code>UserManager</code> instance to serialize
	 */
	public static void serialize(UserManager userManager) {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(
					new FileOutputStream(storeDir + File.separator + storeFile));
			oos.writeObject(userManager);
			oos.close();
		} catch (IOException e) {

		}
	}

	/**
	 * Static function to deserialize a <code>UserManager</code> instance
	 * 
	 * @return <code>UserManager</code> instance saved in the serialized file
	 */
	public static UserManager deserialize() {
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(storeDir + File.separator + storeFile));
			UserManager userManager = (UserManager) ois.readObject();
			ois.close();
			return userManager;
		} catch (ClassNotFoundException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}

		UserManager userManager = new UserManager();
		return userManager;
	}
}
