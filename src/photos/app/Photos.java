// Jeffrey Yang, Yuxiang Wang

package photos.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import photos.controller.AlbumSelectController;
import photos.controller.LoginController;
import photos.controller.ManageAlbumController;
import photos.controller.SearchController;
import photos.controller.UserController;

public class Photos extends Application {
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader();
		UserManager userManager = UserManager.deserialize();
		
		loader.setLocation(getClass().getResource("/photos/view/login.fxml"));
		AnchorPane root = (AnchorPane) loader.load();
		LoginController controller = loader.getController();
		controller.start(primaryStage, userManager);
		Scene scene = new Scene(root, 350, 300);

	    primaryStage.setScene(scene);
		primaryStage.setTitle("Login");
		primaryStage.setResizable(false);
		
		// on close listener
		primaryStage.setOnCloseRequest(event -> {
			UserManager.serialize(userManager);
		});
		primaryStage.show();
		
	}

	public static void main(String args[]) {
		launch(args);
	}
	
	public static void switchToLogin(Stage stage, UserManager userManager) {
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Photos.class.getResource("/photos/view/login.fxml"));
        try {
            AnchorPane branch = (AnchorPane)loader.load();
			LoginController controller = loader.getController();
	        Stage window = stage;
			controller.start(window, userManager); 
	
	        Scene scene = new Scene(branch, 350, 300);
	        window.setTitle("Login");
	        window.setScene(scene);
	        window.setResizable(false);
	        window.show();
        }catch(Exception e) {
        	e.printStackTrace();
        }
	}
	
	public static void switchToManageUsers(Stage stage, UserManager userManager) {
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Photos.class.getResource("/photos/view/manageUsers.fxml"));
        try {
        	AnchorPane branch = (AnchorPane)loader.load();
			UserController controller = loader.getController();
	        Stage window = stage;
			controller.start(window, userManager); 
	
	        Scene scene = new Scene(branch, 350, 350);
	        window.setTitle("Manage Users");
	        window.setScene(scene);
	        window.setResizable(false);
	        window.show();
        }catch(Exception e) {
        	e.printStackTrace();
        }
	}
	
	public static void switchToAlbumSelect(Stage stage, User user, UserManager userManager) {
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Photos.class.getResource("/photos/view/albumSelect.fxml"));
        try {
	        SplitPane branch = (SplitPane)loader.load();
			AlbumSelectController controller = loader.getController();
			controller.start(stage, user, userManager); 
			
			Scene scene = new Scene(branch, 550, 500);
	        stage.setTitle("Album Select");
	        stage.setScene(scene);
	        stage.setResizable(false);
	        stage.show();
        }catch(Exception e) {
        	e.printStackTrace();
        }        
	}
	
	public static void switchToManageAlbum(Stage stage, User user, Album album, UserManager userManager) {
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Photos.class.getResource("/photos/view/manageAlbum.fxml"));
        try {
            SplitPane branch = (SplitPane)loader.load();
			ManageAlbumController controller = loader.getController();
	        Stage window = stage;
			controller.start(window, user, album, userManager); 
	
	        Scene scene = new Scene(branch, 750, 700);
	        window.setTitle("Manage Album");
	        window.setScene(scene);
	        window.setResizable(false);
	        window.show();
        }catch(Exception e) {
        	e.printStackTrace();
        }
	}

	public static void switchToSearch(Stage stage, User user, UserManager userManager) {
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Photos.class.getResource("/photos/view/searchPhotos.fxml"));
        try {
            AnchorPane branch = (AnchorPane)loader.load();
			SearchController controller = loader.getController();
	        Stage window = stage;
			controller.start(window, user, userManager); 
	
	        Scene scene = new Scene(branch, 600, 700);
	        window.setTitle("Search Photos");
	        window.setScene(scene);
	        window.setResizable(false);
	        window.show();
        }catch(Exception e) {
        	e.printStackTrace();
        }
	}

	public static void showError(Stage stage, String title, String text) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.initOwner(stage);
		alert.setTitle(title);
		alert.setHeaderText(text);

		alert.showAndWait();
		return;
	}
}
