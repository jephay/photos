/**
 * @author Jeffrey Yang, Yuxiang Wang
 */
package photos.app;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javafx.scene.image.Image;

/**
 * Class to represent a photo
 */
public class Photo implements Serializable {
	private static final long serialVersionUID = -7613276409710417357L;

	/**
	 * <code>Calendar</code> instance to store the photo's date
	 */
	Calendar cal;

	/**
	 * string indicating the photo's absolute path
	 */
	String path;

	/**
	 * string indicating the photo's filename
	 */
	String filename;

	/**
	 * string indicating the photo's caption
	 */
	String caption;

	/**
	 * <code>HashMap</code> storing the photo's list of tags
	 */
	HashMap<String, ArrayList<String>> tags;

	/**
	 * integer indicating the number of albums the photo is in
	 */
	int albumCount;

	/**
	 * Default <code>Photo</code> constructor
	 * 
	 * @param filepath filepath of the photo to initialize
	 * @param caption  initial caption of photo
	 */
	public Photo(String filepath, String caption) {
		File file = new File(filepath);
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(file.lastModified());
		this.cal = cal;
		this.path = file.getAbsolutePath();
		this.filename = file.getName();
		this.caption = caption;
		this.tags = new HashMap<String, ArrayList<String>>();
		this.albumCount = 0;
	}

	/**
	 * Getter function to get filename of photo
	 * 
	 * @return the photo's filename as a string
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * Getter function to get the date of the photo
	 * 
	 * @return the photo's date as a <code>Date</code> instance
	 */
	public Date getDate() {
		return cal.getTime();
	}

	/**
	 * Getter function to get the date and time of the photo
	 * 
	 * @param showTime boolean indicating whether to include the time of the photo
	 * @return timestamp of the photo as a string
	 */
	public String getTimestamp(boolean showTime) {
		SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");
		if (showTime)
			format1 = new SimpleDateFormat("MM/dd/yyyy hh:mmaa");

		String formatted = format1.format(cal.getTime());

		return formatted;
	}

	/**
	 * Getter function to get the caption of the photo
	 * 
	 * @return the caption of the photo as a string
	 */
	public String getCaption() {
		if (caption.length() > 0)
			return caption;
		else
			return "<No caption>";
	}

	/**
	 * Setter function to set the caption of the photo
	 * 
	 * @param newCaption string indicating the caption to set for the photo
	 */
	public void setCaption(String newCaption) {
		caption = newCaption;
	}

	/**
	 * Function to check if a tag key-value pair already exists for the photo
	 * 
	 * @param name  string indicating the name of the tag
	 * @param value string indicating the value of the tag
	 * @return boolean indicating whether the tag key-value pair already exists
	 */
	public boolean containsTag(String name, String value) {
		ArrayList<String> values = tags.get(name);
		if (values == null)
			return false;

		return values.contains(value);
	}

	/**
	 * Getter function to get all the photo's tags
	 * 
	 * @return <code>ArrayList</code> of <code>Tag</code> instances, representing
	 *         each of the photo's tags
	 */
	public ArrayList<Tag> getTags() {
		ArrayList<Tag> tagList = new ArrayList<Tag>();
		for (HashMap.Entry<String, ArrayList<String>> entry : tags.entrySet()) {
			Tag tag = new Tag(entry.getKey(), entry.getValue());
			tagList.add(tag);
		}
		return tagList;
	}

	/**
	 * Function to update the list of tags for the photo
	 * 
	 * @param updatedTags <code>ArrayList</code> of <code>Tag</code> instances
	 *                    representing the updated list of tags
	 * @param user        <code>User</code> instance indicating the owner of the
	 *                    photo
	 */
	public void updateTags(ArrayList<Tag> updatedTags, User user) {
		tags = new HashMap<String, ArrayList<String>>();
		for (Tag tag : updatedTags) {
			String tagName = tag.getName();
			user.addTag(tagName);
			for (String value : tag.getValues()) {
				addTag(tagName, value);
			}
		}
	}

	/**
	 * Function to add a tag to the photo's list of tags
	 * 
	 * @param name  string indicating the name of the tag to add
	 * @param value string indicating the value of the tag to add
	 */
	public void addTag(String name, String value) {
		if (!tags.containsKey(name)) {
			tags.put(name, new ArrayList<String>());
		}

		tags.get(name).add(value);
	}

	/**
	 * Function to remove a tag from the photo's list of tags
	 * 
	 * @param name  string indicating the name of the tag to remove
	 * @param value string indicating the value of the tag to remove
	 */
	public void removeTag(String name, String value) {
		tags.get(name).remove(name);
	}

	/**
	 * Getter function to get the image of the photo
	 * 
	 * @return <code>Image</code> instance representing the photo's image
	 */
	public Image getImage() {
		File file = new File(path);
		Image image = new Image(file.toURI().toString(), 100, 100, false, false);
		return image;
	}

	/**
	 * Getter function to get the absolute path of the photo's file
	 * 
	 * @return string indicating the photo's absolute path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Getter function to get the number of albums the photo is in
	 * 
	 * @return integer indicating the number of albums the photo is in
	 */
	public int getCount() {
		return albumCount;
	}

	/**
	 * Function to decrement the photo's album count
	 */
	public void decrementCount() {
		albumCount -= 1;
	}

	/**
	 * Function to increment the photo's album count
	 */
	public void incrementCount() {
		albumCount += 1;
	}

	/**
	 * Inner class to represent a photo's tag
	 */
	public static class Tag {
		/**
		 * string indicating the tag's name
		 */
		String name;
		/**
		 * <code>ArrayList</code> of strings storing all the tag values with the same
		 * tag name
		 */
		ArrayList<String> values;

		/**
		 * Default <code>Tag</code> constructor
		 * 
		 * @param name   string indicating the tag's name
		 * @param values <code>ArrayList</code> of strings indicating the values
		 *               corresponding to the tag name
		 */
		public Tag(String name, ArrayList<String> values) {
			this.name = name;
			this.values = values;
		}

		/**
		 * Getter function to get the name of the tag
		 * 
		 * @return string indicating the name of the tag
		 */
		public String getName() {
			return name;
		}

		/**
		 * Getter function to get the list of values for the tag
		 * 
		 * @return <code>ArrayList</code> of the tag's values as strings
		 */
		public ArrayList<String> getValues() {
			return values;
		}

		/**
		 * Function to add a new value to the tag
		 * 
		 * @param newValue string indicating the new value to add
		 */
		public void addValue(String newValue) {
			values.add(newValue);
		}

		/**
		 * Function to remove a value from the tag
		 * 
		 * @param removedValue string indicating the value to remove
		 */
		public void removeValue(String removedValue) {
			values.remove(removedValue);
		}
	}
}
