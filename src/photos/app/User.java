/**
 * @author Jeffrey Yang, Yuxiang Wang
 */
package photos.app;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Class representing a user
 */
public class User implements Serializable {
	private static final long serialVersionUID = 1707653556219341665L;

	/**  
	 * string indicating the username of the user
	 */ 
	String username;

	/**
	 * string indicating the password of the user
	 */
	String password;

	/**
	 * list of <code>Album</code> instances the user owns
	 */
	ArrayList<Album> albums;

	/**
	 * hashmap holding all the photos the user owns, mapping the photo's absolute
	 * path to the photo's <code>Photo</code> instance
	 */
	HashMap<String, Photo> photos;

	/**
	 * hashset of tag names the user has used
	 */
	HashSet<String> tags;

	/**
	 * Default <code>User</code> constructor
	 * 
	 * @param username string indicating the user's username
	 * @param password string indicating the user's password
	 */
	public User(String username, String password) {
		this.username = username;
		this.password = password;
		this.albums = new ArrayList<Album>();
		this.photos = new HashMap<String, Photo>();
		this.tags = new HashSet<String>();
		addTag("location");
		addTag("person");
	}

	/**
	 * toString function for <code>User</code>
	 * 
	 * @return string representing the user's username
	 */
	public String toString() {
		return username;
	}
	
	/**
	 * Getter function to get the user's password
	 * @return string representing the user's password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Getter function to get all of the user's albums
	 * 
	 * @return list of <code>Album</code> instances owned by the user
	 */
	public ArrayList<Album> getAlbums(){
		return albums;
	}

	/**
	 * Function to add an album to the user's list of albums
	 * 
	 * @param newAlbum <code>Album</code> instance to add to the user
	 */
	public void addAlbum(Album newAlbum) {
		albums.add(newAlbum);
	}

	/**
	 * Function to remove an album from the user's list of albums
	 * 
	 * @param removedAlbum <code>Album</code> instance to remove from the user
	 */
	public void removeAlbum(Album removedAlbum) {
		albums.remove(removedAlbum);
		for(Photo photo : removedAlbum.getPhotos()) {
			removedAlbum.removePhoto(photo);
			if(photo.getCount() == 0)
				removePhoto(photo);
		}
	}

	/**
	 * Getter function to get the user's list of photos
	 * 
	 * @return list of <code>Photo</code> instances owned by the user
	 */
	public ArrayList<Photo> getPhotos() {
		Collection<Photo> values = photos.values();
		return new ArrayList<Photo>(values);
	}

	/**
	 * Function to add a photo to the user's list of photos
	 * 
	 * @param newPhoto <code>Photo</code> instance to add to the user
	 * 
	 * @return boolean indicating whether the photo could be added
	 */
	public boolean addPhoto(Photo newPhoto) {
		// check if the photo was already added by the user
		if (photos.containsKey(newPhoto.getPath()))
			return false;

		photos.put(newPhoto.getPath(), newPhoto);
		return true;
	}

	/**
	 * Function to remove a photo from the user's list of photos
	 * @param removedPhoto <code>Photo</code> instance to remove from the user
	 */
	public void removePhoto(Photo removedPhoto) {
		photos.remove(removedPhoto.getPath());
	}

	/**
	 * Getter function to get the user's list of used tag names
	 * @return list of strings indicating the tag names the user has used
	 */
	public ArrayList<String> getTags() {
		return new ArrayList<String>(tags);
	}

	/**
	 * Function to add a tag to the user's list of used tag names
	 * @param tag string indicating the name of the tag
	 */
	public void addTag(String tag) {
		tags.add(tag);
	}
}
	
