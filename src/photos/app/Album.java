/**
 * @author Jeffrey Yang, Yuxiang Wang
 */
package photos.app;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Class representing a photo album
 */
public class Album implements Serializable {
	private static final long serialVersionUID = 8476533542940894831L;

	/**
	 * string indicating the name of the album
	 */
	String name;

	/**
	 * list of <code>Photo</code> instances inside of the album
	 */
	ArrayList<Photo> photos;

	/**
	 * default <code>Album</code> constructor
	 * 
	 * @param name string indicating the name of the album
	 */
	public Album(String name) {
		this.name = name;
		this.photos = new ArrayList<Photo>();
	}

	/**
	 * toString function for <code>Album</code>
	 * 
	 * @return string representing the album's name
	 */
	public String toString() {
		return name;
	}

	/**
	 * Setter function to set the album's name
	 * 
	 * @param newName string indicating the new name of the album
	 */
	public void setName(String newName) {
		name = newName;
	}

	/**
	 * Function to get the range of dates of the photos in the album
	 * 
	 * @return array with the earliest and latest dates in the album as
	 *         <code>Date</code> instances
	 */
	public Date[] getDateRange() {
		Date[] range = new Date[2];
		for (Photo photo : photos) {
			Date photoDate = photo.getDate();
			if (range[0] == null || photoDate.before(range[0]))
				range[0] = photoDate;
			if (range[1] == null || photoDate.after(range[1]))
				range[1] = photoDate;
		}
		return range;
	}

	/**
	 * Getter function to get the list of photos in the album
	 * @return list of <code>Photo</code> instances stored in the album
	 */
	public ArrayList<Photo> getPhotos() {
		return photos;
	}

	/**
	 * Function to add a new photo to the album
	 * @param newPhoto <code>Photo</code> instance to add to the album
	 */
	public void addPhoto(Photo newPhoto) {
		photos.add(newPhoto);
		newPhoto.incrementCount();
	}

	/**
	 * Function to remove a photo from the album
	 * @param removedPhoto <code>Photo</code> instance to remove from the album
	 */
	public void removePhoto(Photo removedPhoto) {
		photos.remove(removedPhoto);
		removedPhoto.decrementCount();
	}
}
